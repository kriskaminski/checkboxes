export interface ICheckbox {
    /** Id of the checkbox. */
    readonly id: string;

    /** Name of the checkbox. */
    readonly name: string;

    /** Value of the checkbox. */
    readonly value?: string;

    /** Specifies if the checkbox is an exclusive value. */
    readonly exclusive?: boolean;
}
