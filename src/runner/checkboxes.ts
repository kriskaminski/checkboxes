/** Dependencies */
import {
    NodeBlock,
    Slots,
    Value,
    assert,
    filter,
    findFirst,
    map,
    processor,
    validator,
} from "tripetto-runner-foundation";
import { ICheckbox } from "./checkbox";

export abstract class Checkboxes extends NodeBlock<{
    checkboxes: ICheckbox[];
    required?: boolean;
}> {
    /** Retrieves if a checked checkbox is required. */
    get required(): boolean {
        return this.props.required || false;
    }

    /** Retrieves a checkbox slot. */
    checkboxSlot(checkbox: ICheckbox): Value<boolean, Slots.Boolean> {
        return assert(this.valueOf<boolean>(checkbox.id));
    }

    /** Retrieves if a checkbox is checked. */
    isChecked(checkbox: ICheckbox): boolean {
        const checkboxSlot = this.checkboxSlot(checkbox);

        return (checkboxSlot && checkboxSlot.confirm().value) || false;
    }

    /** Sets a checkbox state. */
    check(checkbox: ICheckbox, checked: boolean): boolean {
        const checkboxSlot = this.checkboxSlot(checkbox);

        if (checkboxSlot) {
            checkboxSlot.value = checked;

            return checkboxSlot.confirm().value;
        }

        return false;
    }

    /** Toggles a checkbox. */
    toggle(checkbox: ICheckbox): void {
        const checkboxSlot = this.checkboxSlot(checkbox);

        if (checkboxSlot) {
            checkboxSlot.value = !checkboxSlot.value;
        }
    }

    @processor
    processExclusive(): void {
        const checkboxes = filter(
            map(this.props.checkboxes, (checkbox) => ({
                id: checkbox.id,
                exclusive: checkbox.exclusive,
                valueRef: assert(this.valueOf<boolean>(checkbox.id)),
            })),
            (checkbox) => checkbox.valueRef.value === true
        ).sort((a, b) => (b.valueRef.time || 0) - (a.valueRef.time || 0));
        const lastChecked = checkboxes.length && checkboxes[0];

        if (lastChecked) {
            checkboxes.forEach((c) => {
                if (
                    c.id !== lastChecked.id &&
                    (lastChecked.exclusive || c.exclusive)
                ) {
                    c.valueRef.value = false;
                }
            });
        }
    }

    @validator
    validate(): boolean {
        if (this.props.required) {
            return findFirst(
                this.props.checkboxes,
                (checkbox: ICheckbox) =>
                    assert(this.valueOf<boolean>(checkbox.id)).value === true
            )
                ? true
                : false;
        }

        return true;
    }
}
