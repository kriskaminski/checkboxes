/** Dependencies */
import {
    Collection,
    Forms,
    Slots,
    affects,
    created,
    definition,
    deleted,
    editor,
    isBoolean,
    isString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
} from "tripetto";
import { Checkboxes } from "./index";
import { ICheckbox } from "../runner";

export class Checkbox extends Collection.Item<Checkboxes> implements ICheckbox {
    @name
    @definition
    name = "";

    @definition
    @affects("#refresh")
    value?: string;

    @definition
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        this.ref.slots.dynamic({
            type: Slots.Boolean,
            reference: this.id,
            label: pgettext("block:checkboxes", "Checkbox"),
            sequence: this.index,
            name: this.name,
            alias: this.value,
            required: this.ref.required,
            exportable: this.ref.exportable,
            pipeable: {
                group: "Checkbox",
                label: pgettext("block:checkboxes", "Checkbox"),
                template: "name",
                alias: this.ref.alias,
            },
        });
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:checkboxes", "Label"),
            form: {
                title: pgettext("block:checkboxes", "Checkbox label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.group(pgettext("tripetto:builder", "Options"));

        this.editor.option({
            name: pgettext("block:checkboxes", "Exclusivity"),
            form: {
                title: pgettext("block:checkboxes", "Checkbox exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:checkboxes",
                            "Uncheck all other checkboxes when checked"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.exclusive),
        });

        this.editor.option({
            name: pgettext("block:checkboxes", "Identifier"),
            form: {
                title: pgettext("block:checkboxes", "Checkbox identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:checkboxes",
                            "If a checkbox identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });
    }
}
