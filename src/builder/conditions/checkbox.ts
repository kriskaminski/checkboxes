/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { Checkboxes } from "../";
import { Checkbox } from "../checkbox";
import ICON from "../../../assets/icon.svg";
import ICON_CHECKED from "../../../assets/checked.svg";
import ICON_UNCHECKED from "../../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Checkboxes,
    icon: ICON,
    get label() {
        return pgettext("block:checkboxes", "Checkbox state");
    },
})
export class CheckboxCondition extends ConditionBlock {
    @affects("#name")
    @definition("checkboxes")
    checkbox: Checkbox | undefined;

    @affects("#icon")
    @definition
    checked = true;

    get icon() {
        return this.checked ? ICON_CHECKED : ICON_UNCHECKED;
    }

    get name() {
        return (this.checkbox ? this.checkbox.name : "") || this.type.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            controls: [
                new Forms.Checkbox(
                    pgettext("block:checkboxes", "Checkbox is checked"),
                    Forms.Checkbox.bind(this, "checked", true)
                ),
            ],
        });
    }
}
